﻿using System.Web.Http;
using System.Web.Http.Cors;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Owin;

namespace Cinema.WebApi
{
    public class Startup
    {
        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{id}",
                new {id = RouteParameter.Optional}
            );

            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));
            appBuilder
                .UseNinjectMiddleware(new NinjectBootstrap().GetKernel)
                .UseNinjectWebApi(config);
        }
    }
}