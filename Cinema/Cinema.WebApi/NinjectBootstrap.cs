﻿using CinemaPlanet.Business;

using Ninject;

namespace Cinema.WebApi
{
    public class NinjectBootstrap
    {
        public IKernel GetKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(new BusinessModule());
            return kernel;
        }
    }
}