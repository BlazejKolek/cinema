﻿using System.Configuration;

namespace Cinema.WebApi
{
    internal interface IAppSettings
    {
        string WebApiUri { get; }
    }

    internal class AppSettings : IAppSettings
    {
        public string WebApiUri => ConfigurationManager.AppSettings["WebApiUri"];
    }
}