﻿using System;

using CinemaPlanet.Business.Helpers;

using Topshelf;

namespace Cinema.WebApi
{
    public class Program
    {
        private static void Main(string[] args)
        {
            AutoMapperModel.CreateMap();
            var rc = HostFactory.Run(x =>
            {
                x.Service<OwinBootstrap>(s =>
                {
                    s.ConstructUsing(name => new OwinBootstrap());
                    s.WhenStarted(owinBootstrap => owinBootstrap.Start());
                    s.WhenStopped(owinBootstrap => owinBootstrap.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("Cinema planet WebApi ");
                x.SetDisplayName("CinemaWebApi");
                x.SetServiceName("CinemaWebApi");
            });

            var exitCode = (int) Convert.ChangeType(rc, rc.GetTypeCode());
            Environment.ExitCode = exitCode;
        }
    }
}