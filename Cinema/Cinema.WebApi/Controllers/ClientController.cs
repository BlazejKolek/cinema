﻿using System.Threading.Tasks;
using System.Web.Http;
using Cinema.Business.Helpers;
using Cinema.Business.Interfaces;
using Cinema.Business.Models.Client;

namespace Cinema.WebApi.Controllers
{
    public class ClientController : ApiController
    {
        private readonly IClientService _clientService;

        public ClientController(IClientService clientService)
        {
            _clientService = clientService;
        }

        public async Task<IHttpActionResult> Post(ClientBl client)
        {
            if (_clientService.IsEmailExist(client.Mail))
            {
                return BadRequest("Email exist in database");
            }

            client.Password = MD5Helper.CreateMD5(client.Password);

            if (!_clientService.CheckDataIsCorrect(client))
            {
                return BadRequest("Incorrect data values");
            }

            await _clientService.AddClientAsync(client);
            return Ok("Client add to database");
        }


    }
}
