﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

using Cinema.Business.Helpers;
using Cinema.Business.Interfaces;
using Cinema.Business.Models;

namespace Cinema.WebApi.Controllers
{
   public class OrderController : ApiController
   {
       private readonly ITicketService _ticketService;
       private readonly IOrderService _orderService;
       private readonly IProgrammService _programmService;
       private readonly IClientService _clientService;

       public OrderController(ITicketService ticketService, IOrderService orderService, IProgrammService programmService,
           IClientService clientService)
       {
            _ticketService = ticketService;
            _orderService = orderService;
            _programmService = programmService;
            _clientService = clientService;
        }

       public async Task<IHttpActionResult> Post(List<TicketBl> tickets)
       {
           if (ValuesComparer.CheckListHasSameValues(tickets))
           {
              return BadRequest("Entry values is not the same");
           }

            var programme = _programmService.ReturnProgrammIfExist(tickets[0].CinemaId, tickets[0].TimeOfSeance,
               tickets[0].MovieId, tickets[0].CinemaHallId);

            if (!_ticketService.CheckClientHavAddTicketToDifferentProgramms(tickets))
            {
                return BadRequest("It's not the same repertoir");
            }

           foreach (var ticket in tickets)
           {
               if (_ticketService.CheckTicketExist(ticket.CinemaId,ticket.MovieId,ticket.TimeOfSeance,ticket.CinemaHallId,ticket.Row,ticket.Seat))
               {
                  return BadRequest("This ticket was sold.");
               }

               if (!_ticketService.SeatAndRowIsExist(ticket.CinemaHallId,ticket.Row,ticket.Seat))
               {
                   return BadRequest($"Place for {ticket.Row} and {ticket.Seat} isn't exsist.");
               }
           }

           foreach (var ticket in tickets)
           {
               ticket.SaleDate = DateTime.Now;
               ticket.Price = _ticketService.ReturnTicketPrice(ticket.TimeOfSeance);
           }
           var order = new OrderBl();
           order.Tickets = tickets;

           await _orderService.AddOrderAsync(order);          
           return Ok("Tickets sold!");
       }
    }


}
