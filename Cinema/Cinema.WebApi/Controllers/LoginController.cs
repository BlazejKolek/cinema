﻿using Cinema.Business.Interfaces;
using Cinema.Business.Models.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Cinema.WebApi.Controllers
{
   public class LoginController : ApiController
    {
        private readonly IClientService _clientService;

        public LoginController(IClientService clientService)
        {
            _clientService = clientService;
        }

        [HttpPost]
        public ClientDetail AuthorizeClient(ClientAuth client)
        {
            return _clientService.AuthorizeClient(client);
        }
    }
}
