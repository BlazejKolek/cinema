﻿using Cinema.Business.Interfaces;
using Cinema.Business.Models;
using Cinema.Business.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Cinema.WebApi.Controllers
{
   public class TicketController : ApiController
    {
        private readonly ITicketService _ticketService;
        public TicketController(ITicketService ticketService)
        {
            _ticketService = ticketService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> TakeAllUserTicket(int id)
        {
            var tickets = await _ticketService.GetUserTicketsAsync(id);
            if (tickets.Count() == 0)
            {
                return BadRequest($"Not found ticket for user id : {id}");
            }

            return Ok(tickets);
        }

        /*
       URL: http://localhost:9000/api/tickets?pageSize=10&page=1&clentId=0
       Method: GET
       */
        public PagedResults<TicketBl> GetTransfersHistory(int pageSize = 3, int page = 1,int clientId = 4)
        {
            return _ticketService.GetTicketPage(pageSize, page,clientId);
        }

    }
}
