﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using CinemaPlanet.Business.Interfaces;
using CinemaPlanet.Business.Models;

namespace Cinema.WebApi.Controllers
{
    public class CinemaController : ApiController
    {
        private readonly ICinemaService _cinemaService;

        public CinemaController(ICinemaService cinemaService)
        {
            _cinemaService = cinemaService;
        }

        public async Task<IHttpActionResult> Post(CinemaBl cinema)
        {
            if (_cinemaService.CheckDataIsCorrect(cinema))
            {
                await _cinemaService.AddCinemaAsync(cinema);
                return Ok("Data add to database");
            }

            return BadRequest("Wrong input data");
        }

        [HttpGet]
        public IEnumerable<CinemaBl> GetCinemas()
        {
            return _cinemaService.GetAllCinemas();
        }
    }
}