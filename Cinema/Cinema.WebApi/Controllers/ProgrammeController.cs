﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

using Cinema.Business.Interfaces;
using Cinema.Business.Models;

namespace Cinema.WebApi.Controllers
{
    public class ProgrammeController : ApiController
    {
        private readonly IProgrammService _programmeService;

        public ProgrammeController(IProgrammService programmService)
        {
            _programmeService = programmService;
        }

        public async Task<IHttpActionResult> Post(ProgrammeBl programme)
        {
            if (_programmeService.CheckEntryDataIsCorrect(programme))
            {
                await _programmeService.AddProgrammeAsync(programme);
                return Ok("Data add to database");
            }

            return BadRequest("Wrong input data");
        }

        [HttpGet]
        public async Task<IHttpActionResult> Get([FromUri] int cinemaId, [FromUri] DateTime date)
        {
            var program = await _programmeService.GetProgrammeAsync(cinemaId, date);

            if (program == null)
                return BadRequest($"The program for the cinema {cinemaId} and the date {date} does not exist");

            return Ok(program);
        }
    }
}