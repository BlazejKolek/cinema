﻿using System.Threading.Tasks;
using System.Web.Http;

using CinemaPlanet.Business.Interfaces;
using CinemaPlanet.Business.Models;

namespace Cinema.WebApi.Controllers
{
    public class MovieController : ApiController
    {
        private readonly IMovieService _movieService;

        public MovieController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        public async Task<IHttpActionResult> Post(MovieBl movie)
        {
            if (_movieService.CheckEntryDataIsCorrect(movie))
            {
                await _movieService.AddMovieAsync(movie);
                return Ok("Data add to database");
            }

            return BadRequest("Wrong input data");
        }
    }
}