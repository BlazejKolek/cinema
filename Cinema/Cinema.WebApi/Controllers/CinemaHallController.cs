﻿using System.Threading.Tasks;
using System.Web.Http;

using CinemaPlanet.Business.Interfaces;
using CinemaPlanet.Business.Models;

namespace Cinema.WebApi.Controllers
{
    public class CinemaHallController : ApiController
    {
        private readonly ICinemaHallService _cinemaHallService;

        public CinemaHallController(ICinemaHallService cinemaHallService)
        {
            _cinemaHallService = cinemaHallService;
        }

        public async Task<IHttpActionResult> Post(CinemaHallBl cinemaHall)
        {
            if (_cinemaHallService.CheckEntryDataIsCorrect(cinemaHall))
            {
                await _cinemaHallService.AddCinemaHallAsync(cinemaHall);
                return Ok("Data add to database");
            }

            return BadRequest("Wrong input data");
        }
    }
}