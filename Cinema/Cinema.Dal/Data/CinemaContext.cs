﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;

using Cinema.Dal.Data.Models;

using CinemaPlanet.Dal.Data.Models;

namespace Cinema.Dal.Data
{
    public class CinemaContext : DbContext, ICinemaContext
    {
        public CinemaContext() : base("CinemaConnectionString")
        {
            
        }

        public Func<CinemaContext> Context { get; }

        public IDbSet<CinemaPlanet.Dal.Data.Models.Cinema> Cinema { get; set; }
        public IDbSet<CinemaHall> CinemaHall { get; set; }
        public IDbSet<Movie> Movie { get; set; }
        public IDbSet<Repertoire> Repertoire { get; set; }
        public IDbSet<Programme> Programme { get; set; }
        public IDbSet<RepertoirHours> Hours { get; set; }
        public IDbSet<Client> Clients { get; set; }
        public IDbSet<Ticket> Tickets { get; set; }
        public IDbSet<Order> Orders { get; set; }

        public void SaveChanges()
        {
            SaveChanges();
        }

        public override Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }
    }
}