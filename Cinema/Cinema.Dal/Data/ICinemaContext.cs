﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

using Cinema.Dal.Data.Models;

using CinemaPlanet.Dal.Data.Models;

namespace Cinema.Dal.Data
{
    public interface ICinemaContext : IDisposable
    {
        IDbSet<CinemaPlanet.Dal.Data.Models.Cinema> Cinema { get; set; }
        IDbSet<CinemaHall> CinemaHall { get; set; }
        IDbSet<Movie> Movie { get; set; }
        IDbSet<Repertoire> Repertoire { get; set; }
        IDbSet<Programme> Programme { get; set; }
        IDbSet<RepertoirHours> Hours { get; set; }
        IDbSet<Client> Clients { get; set; }
        IDbSet<Ticket> Tickets { get; set; }
        IDbSet<Order> Orders { get; set; }

        Task<int> SaveChangesAsync();
        void SaveChanges();
        DbEntityEntry<T> Entry<T>(T entity) where T : class;
    }
}