﻿using Ninject.Modules;

namespace Cinema.Dal.Data
{
    public class DalModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<ICinemaContext>().ToMethod(x => new CinemaContext());
        }
    }
}