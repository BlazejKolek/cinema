﻿namespace CinemaPlanet.Dal.Data.Models
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public Genre Genre { get; set; }
        public int Runtime { get; set; }
        public string ShortDescription { get; set; }

        public string FilmwebPage { get; set; }
    }
}