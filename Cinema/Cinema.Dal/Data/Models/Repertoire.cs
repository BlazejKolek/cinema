﻿using System;
using System.Collections.Generic;

using Cinema.Dal.Data.Models;

namespace CinemaPlanet.Dal.Data.Models
{
    public class Repertoire
    {
        public int Id { get; set; }
        public Movie Movie { get; set; }
        public CinemaHall Hall { get; set; }
        public List<RepertoirHours> Hours { get; set; }
    }
}