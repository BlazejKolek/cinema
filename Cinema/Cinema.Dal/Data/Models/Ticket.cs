﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.Dal.Data.Models
{
   public class Ticket
    {
        public int Id { get; set; }
        public string UserEmail { get; set; }
        public int CinemaId { get; set; }
        public int MovieId { get; set; }
        public int CinemaHallId { get; set; }
        public string Row { get; set; }
        public int Seat { get; set; }
        public DateTime TimeOfSeance { get; set; }
        public DateTime SaleDate { get; set; }
        public double Price { get; set; }
    }
}
