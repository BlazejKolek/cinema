﻿namespace CinemaPlanet.Dal.Data.Models
{
    public class CinemaHall
    {
        public int Id { get; set; }
        public Cinema Cinema { get; set; }
        public int Rows { get; set; }
        public int Seats { get; set; }
    }
}