﻿using System;

using CinemaPlanet.Dal.Data.Models;

namespace Cinema.Dal.Data.Models
{
    public class Programme
    {
        public int Id { get; set; }
        public CinemaPlanet.Dal.Data.Models.Cinema Cinema { get; set; }
        public DateTime StartOfPlaying { get; set; }
        public DateTime EndOfPlaying { get; set; }
        public Repertoire Repertoire { get; set; }
    }
}