﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.Dal.Data.Models
{
   public class Order
    {
        public int Id { get; set; }
        public List<Ticket> Tickets { get; set; }
        public double SumOfOrder { get; set; }
    }
}
