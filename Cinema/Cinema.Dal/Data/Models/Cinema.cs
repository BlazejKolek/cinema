﻿using System;

namespace CinemaPlanet.Dal.Data.Models
{
    public class Cinema
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Mail { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime OpeningHour { get; set; }
    }
}