﻿using Cinema.Dal.Data;
using CinemaPlanet.Business.Interfaces;
using CinemaPlanet.Business.Models;
using CinemaPlanet.Dal.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Cinema.Business.Models;

namespace CinemaPlanet.Business.Services
{
    public class MovieService : IMovieService
    {
        const string filmwebRegex = @"^((https?):\/\/)?(www.)?([filmweb.pl]+)(\/)[\d\w]*";
        private readonly Func<ICinemaContext> _cinemaContext;

        public MovieService(Func<ICinemaContext> cinemaContext)
        {
            _cinemaContext = cinemaContext;
        }

        public async Task AddMovieAsync(MovieBl movie)
        {
            using (var context = _cinemaContext())
            {
                context.Movie.Add(AutoMapper.Mapper.Map<MovieBl, Movie>(movie));
                await context.SaveChangesAsync();
            }
        }

        public bool CheckEntryDataIsCorrect(MovieBl movie)
        {
            if (movie == null || string.IsNullOrWhiteSpace(movie.Title) 
                              || movie.Runtime.Equals(0) 
                              || string.IsNullOrWhiteSpace(movie.ShortDescription)
                              || !IsFilmwebAddress(movie.FilmwebPage))
            {
                return false;
            }
            return true;
        }

        public IEnumerable<MovieBl> GetAllMovies()
        {
            using (var context = _cinemaContext())
            {
                return AutoMapper.Mapper.Map<List<Movie>,List<MovieBl>>(context.Movie.ToList());
            }
        }

        public MovieBl ReturnMovieById(int id)
        {
            using (var context = _cinemaContext())
            {
                return AutoMapper.Mapper.Map<Movie,MovieBl>(context.Movie.FirstOrDefault(x => x.Id == id));
            }
        }

        private bool IsFilmwebAddress(string address)
        {
            return Regex.IsMatch(address, filmwebRegex);
        }

        public GenreBl IsGenre(string genre)
        {
            var genreToCheckExist = Enum.IsDefined(typeof(GenreBl), genre);
            if (genreToCheckExist)
            {
               var returnedGenre = (GenreBl)Enum.Parse(typeof(GenreBl), genre);
               return returnedGenre;
            }
            return 0;
        }
    }
}
