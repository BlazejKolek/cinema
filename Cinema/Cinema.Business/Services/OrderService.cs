﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cinema.Business.Helpers;
using Cinema.Business.Interfaces;
using Cinema.Business.Models;
using Cinema.Dal.Data;
using Cinema.Dal.Data.Models;

namespace Cinema.Business.Services
{
   public class OrderService : IOrderService
   {
       private readonly Func<ICinemaContext> _cinemaContext;
            
       public OrderService(Func<ICinemaContext> cinemaContext, ITicketService ticketService)
       {
            _cinemaContext = cinemaContext;
        }


        public async Task AddOrderAsync(OrderBl order)
        {
            if (CheckDataIsCorrect(order))
            {
                using (var context = _cinemaContext())
                {
                    order.Tickets = ChangePriceWhenPromotionIsAvailabel(order.Tickets);

                    order.SumOfOrder = order.Tickets.Sum(x => x.Price);

                    context.Orders.Add(AutoMapper.Mapper.Map<OrderBl, Order>(order));
                    await context.SaveChangesAsync();

                    var mail = new EmailSender(_cinemaContext);
                    mail.SendEmail(order);

                }
            }
        }

        public List<TicketBl> ChangePriceWhenPromotionIsAvailabel(List<TicketBl> tickets)
        {

            using (var context = _cinemaContext())
            {
                var ticketData = tickets.FirstOrDefault();
                var howManyTicketsSold = context.Tickets.Where(x => x.UserEmail == ticketData.UserEmail).Count();

                var howManyTicketsSoldInCurrentMont = context.Tickets.Where(x =>
                    x.UserEmail == ticketData.UserEmail && x.SaleDate.Month == ticketData.SaleDate.Month
                                                    && x.SaleDate.Year == ticketData.SaleDate.Year).Count();

                var howManyTicketsClientsWantToAdd = tickets.Count;


                if (howManyTicketsSold + tickets.Count >= 10 || (howManyTicketsSoldInCurrentMont + tickets.Count) > 5)
                {
                    for (int i = 0; i < tickets.Count; i++)
                    {
                        if (i + 1 + howManyTicketsSold == 10)
                        {
                            tickets[i].Price = 0;
                        }

                        if (i + 1 + howManyTicketsSoldInCurrentMont > 5)
                        {
                            tickets[i].Price = Math.Round(tickets[i].Price * 0.8, 2);
                        }

                    }
                }
            }

            return tickets;
        }

        public bool CheckDataIsCorrect(OrderBl order)
        {
            if (order == null
                || DontHaveAnyTicketsInOrder(order.Tickets))
            {
                return false;
            }
            return true;
        }

        public bool DontHaveAnyTicketsInOrder(List<TicketBl> tickets)
        {
            if (tickets == null)
            {
                return true;
            }

            return false;
        }
    }
}
