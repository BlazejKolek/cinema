﻿using Cinema.Dal.Data;
using CinemaPlanet.Business.Interfaces;
using CinemaPlanet.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Cinema.Business.Helpers;

namespace CinemaPlanet.Business.Services
{
    public class CinemaService : ICinemaService
    {

        private readonly Func<ICinemaContext> _cinemaContext;

        public CinemaService(Func<ICinemaContext> cinemaContext)
        {
            _cinemaContext = cinemaContext;
        }

        public async Task AddCinemaAsync(CinemaBl cinema)
        {
                using (var context = _cinemaContext())
                {
                    context.Cinema.Add(AutoMapper.Mapper.Map<CinemaBl, CinemaPlanet.Dal.Data.Models.Cinema>(cinema));
                    await context.SaveChangesAsync();
                }
        }

        public bool CheckDataIsCorrect(CinemaBl cinema)
        {
            if (string.IsNullOrWhiteSpace(cinema.Name) || string.IsNullOrWhiteSpace(cinema.Address)
                                                       || !PhoneValidator.IsValidTelephoneNumber(cinema.PhoneNumber)
                                                       || !EmailValidator.IsEmailValid(cinema.Mail)
                                                       || cinema.OpeningHour == DateTime.MinValue)
            {
                return false;
            }
            return true;
        }

        public IEnumerable<CinemaBl> GetAllCinemas()
        {
            using (var context = _cinemaContext())
            {
                return AutoMapper.Mapper.Map<List<CinemaPlanet.Dal.Data.Models.Cinema>, List<CinemaBl>>
                                            (context.Cinema.ToList());
            }
        }

        public CinemaBl GetCinemaById(int cinemaId)
        {
            using (var context = _cinemaContext())
            {
                return AutoMapper.Mapper.Map<CinemaPlanet.Dal.Data.Models.Cinema, CinemaBl>
                    (context.Cinema.FirstOrDefault(x => x.Id == cinemaId));
            }
        }

        public DateTime ReturnOpeningHour(CinemaBl cinema)
        {
            using (var context = _cinemaContext())
            {
                return AutoMapper.Mapper.Map<CinemaPlanet.Dal.Data.Models.Cinema, CinemaBl>
                    (context.Cinema.FirstOrDefault(x => x.Id == cinema.Id)).OpeningHour;
            }
        }
    }
}
