﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cinema.Business.Helpers;
using Cinema.Business.Interfaces;
using Cinema.Business.Models;
using Cinema.Dal.Data;
using Cinema.Dal.Data.Models;

namespace Cinema.Business.Services
{
   public class TicketService : ITicketService
    {
       private readonly Func<ICinemaContext> _cinemaContext;
       private readonly IProgrammService _programmService;
       private readonly IClientService _clientService;

       public TicketService(Func<ICinemaContext> cinemaContext,IProgrammService programmService, IClientService clientService)
       {
            _cinemaContext = cinemaContext;
            _programmService = programmService;
            _clientService = clientService;
        }

        public TicketBl CreateTicket(int cinemaId,string userEmail,int movieId, DateTime date, int cinemaHallId, string row,
            int seat)
        {
            if (CheckEnteredDataIsCorrect(cinemaId,userEmail,movieId,date,cinemaHallId,row,
            seat))
            {
                var ticket = new TicketBl()
                {
                    CinemaId = cinemaId,
                    MovieId = movieId,
                    UserEmail = userEmail,
                    CinemaHallId = cinemaHallId,
                    Row = row,
                    Seat = seat,
                    TimeOfSeance = date,
                    SaleDate = DateTime.Now,
                    Price = ReturnTicketPrice(date)
                };

                return ticket;
            }
            return null;
        }

        private bool CheckEnteredDataIsCorrect(int cinemaId, string userEmail, int movieId, DateTime date, int cinemaHallId, string row, int seat)
        {
            if (string.IsNullOrWhiteSpace(userEmail)
            || !SeatAndRowIsExist(cinemaHallId,row,seat))
            {
                return false;
            }

            return true;
        }

        public bool SeatAndRowIsExist(int cinemaHallId, string row, int seat)
        {
            using (var context = _cinemaContext())
            {
                var cinemaHall = context.CinemaHall.Where(x => x.Id == cinemaHallId).FirstOrDefault();
                var countOfRows = cinemaHall.Rows;
                var countOfSeats = cinemaHall.Seats;

                char[] alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();

                var rowsUserWant = Convert.ToChar(row);

                if (rowsUserWant > alpha[countOfRows] - 1 || seat > countOfSeats)
                {
                    return false;
                }

                return true;
            }
        }

        public bool CheckTicketExist(int cinemaId, int movieId, DateTime date, int cinemaHallId,
                                     string row,
                                     int seat)
        {
            using (var context = _cinemaContext())
            {
                return context.Tickets.Where(x => x.CinemaHallId == cinemaHallId
                                                             && x.CinemaId == cinemaId
                                                             && x.MovieId == movieId
                                                             && x.TimeOfSeance == date
                                                             && x.Row == row
                                                             && x.Seat == seat).Any();
              
            }
        }

        public bool CheckUserHaveAddSameTicket(List<TicketBl> tickets)
        {
            if (tickets.Count() == 1)
            {
                return false;
            }

            for (int i = 0; i < tickets.Count - 1; i++)
            {
                for (int j = i + 1; j < tickets.Count; j++)
                {
                    if (tickets[i].CinemaHallId == tickets[j].CinemaHallId 
                        && tickets[i].Seat == tickets[j].Seat
                        && tickets[i].Row == tickets[j].Row
                        && tickets[i].CinemaId == tickets[j].CinemaId
                        && tickets[i].TimeOfSeance == tickets[j].TimeOfSeance
                        && tickets[i].UserEmail == tickets[j].UserEmail)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool CheckClientHavAddTicketToDifferentProgramms(List<TicketBl> tickets)
        {
            if (tickets.Count() == 1)
            {
                return false;
            }

            for (int i = 0; i < tickets.Count - 1; i++)
            {
                for (int j = i + 1; j < tickets.Count; j++)
                {
                    if (tickets[i].CinemaHallId == tickets[j].CinemaHallId
                        && tickets[i].CinemaId == tickets[j].CinemaId
                        && tickets[i].TimeOfSeance == tickets[j].TimeOfSeance
                        && tickets[i].UserEmail == tickets[j].UserEmail)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool CheckClientsAddedIsTheSame(List<string> clients)
        {
            return ValuesComparer.CheckListHasSameValues(clients);
        }


        public int ReturnTicketPrice(DateTime date)
        {
            if (date.DayOfWeek >= DayOfWeek.Monday && date.DayOfWeek <= DayOfWeek.Thursday)
            {
                return 20;
            }

            return 25;
        }

        public async Task<IEnumerable<TicketBl>> GetUserTicketsAsync(int id)
        {
            using (var context = _cinemaContext())
            {
                var client = await context.Clients.Where(x => x.Id == id).Select(y => y.Mail).FirstOrDefaultAsync();

                return AutoMapper.Mapper.Map<List<Ticket>, List<TicketBl>>(await context.Tickets
                    .Where(x => x.UserEmail == client)
                    .ToListAsync());
            }
        }

        public PagedResults<TicketBl> GetTicketPage(int pageSize,int page,int clientId)
        {

            using (var context = _cinemaContext())
            {
                var client = context.Clients.FirstOrDefault(x => x.Id == clientId).Mail;
                var resultCount = context.Tickets.Where(x => x.UserEmail == client).Count();

                var pagesCount = (int)Math.Ceiling((double)resultCount / pageSize);

                var resultItem = context.Tickets
                    .Where(x => x.UserEmail == client)
                    .OrderBy(x => x.SaleDate)
                    .Skip(pageSize * (page - 1))
                    .Take(pageSize)                  
                    .Select(
                    x => new TicketBl
                    {
                        UserEmail = x.UserEmail,
                        Id = x.Id,
                        CinemaHallId = x.CinemaHallId,
                        CinemaId = x.CinemaId,
                        MovieId = x.MovieId,
                        Price = x.Price,
                        Row = x.Row,
                        SaleDate = x.SaleDate,
                        Seat = x.Seat,
                        TimeOfSeance = x.TimeOfSeance
                    }
                    ).ToList();

                var results = new PagedResults<TicketBl>
                {
                    Page = page,
                    PageSize = pageSize,
                    ResultsCount = resultCount,
                    PagesCount = (int)Math.Ceiling((double)resultCount / pageSize),
                    NextPageAttributes = (pagesCount <= page) ? null : $"?pageSize={pageSize}&page={page + 1}&clientId={clientId}",
                    PrevPageAttributes = (page <= 1) ? null : $"?pageSize={pageSize}&page={page - 1}&clientId={clientId}",
                    Results = resultItem

                };

                return results;

            }
        }

    }
}
