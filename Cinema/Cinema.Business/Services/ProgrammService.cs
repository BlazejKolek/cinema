﻿using Cinema.Business.Interfaces;
using Cinema.Business.Models;
using Cinema.Dal.Data;
using Cinema.Dal.Data.Models;
using CinemaPlanet.Business.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Threading.Tasks;
using CinemaPlanet.Business.Interfaces;

namespace Cinema.Business.Services
{
    public class ProgrammService : IProgrammService
    {
        private readonly Func<ICinemaContext> _cinemaContext;
        private readonly IRepertoirService _repertoirService;
        private readonly ICinemaService _cinemaService;

        public ProgrammService(Func<ICinemaContext> cinemaContext, IRepertoirService repertoirService, ICinemaService cinemaService)
        {
            _cinemaContext = cinemaContext;
            _repertoirService = repertoirService;
            _cinemaService = cinemaService;
        }

        public async Task AddProgrammeAsync(ProgrammeBl programme)
        {
            var programmeMappedToDatabase = AutoMapper.Mapper.Map<ProgrammeBl, Programme>(programme);

            using (var context = _cinemaContext())
            {
                context.Cinema.Attach(programmeMappedToDatabase.Cinema);
                context.Entry(programmeMappedToDatabase.Cinema).State = EntityState.Unchanged;

                context.Movie.Attach(programmeMappedToDatabase.Repertoire.Movie);
                context.Entry(programmeMappedToDatabase.Repertoire.Movie).State = EntityState.Unchanged;

                programmeMappedToDatabase.Repertoire.Hall =
                    context.CinemaHall.FirstOrDefault(x => x.Id == programme.Repertoire.Hall.Id);
                programmeMappedToDatabase.Repertoire.Hall.Cinema =
                    context.Cinema.FirstOrDefault(x => x.Id == programme.Cinema.Id);

                foreach (var hour in programme.Repertoire.Hours)
                {                  
                    context.Hours.Add(AutoMapper.Mapper.Map<RepertoirHoursBl, RepertoirHours>(hour));
                }

                context.Programme.Add(programmeMappedToDatabase);
                
                await context.SaveChangesAsync();
            }
        }

        public IEnumerable<ProgrammeBl> GetAllProgramme()
        {
            using (var context = _cinemaContext())
            {
                return AutoMapper.Mapper
                      .Map<List<Programme>, List<ProgrammeBl>>(context.Programme
                      .Include(x => x.Cinema)
                      .Include(y => y.Repertoire.Movie)
                      .Include(z => z.Repertoire.Hall)
                      .Include(u => u.Repertoire.Hours.Select(y => y.Hour))
                      .ToList());
            }
        }

        public async Task<ProgrammeBl> GetProgrammeAsync(int cinema, DateTime date)
        {
            var startingWeek = StartOfPlayingRepertoir(date);
            using (var context = _cinemaContext())
            {
                return AutoMapper.Mapper.Map<Programme, ProgrammeBl>(await context.Programme
                    .Include(a => a.Cinema)
                    .Include(x => x.Repertoire)
                    .Include(z => z.Repertoire.Hall)
                    .Include(u => u.Repertoire.Hours)
                    .Include(b => b.Repertoire.Movie)
                    .Where(x => x.Cinema.Id == cinema && x.StartOfPlaying == startingWeek)
                    .FirstOrDefaultAsync());
            }
        }     

        public bool CheckEntryDataIsCorrect(ProgrammeBl programme)
        {
            if (programme == null
                || programme.StartOfPlaying.Equals(DateTime.MinValue)
                || programme.Cinema.Id.Equals(0)
                || !CheckAllEntryHoursIsNotBeforeOpenCinema(programme)
                || !CheckAllHoursGivenIsOverlapping(programme)
                )
            {
                return false;
            }
            return true;
        }

        public bool CheckAllHoursGivenIsOverlapping(ProgrammeBl programme)
        {
            var openingHour = _cinemaService.ReturnOpeningHour(programme.Cinema);
            var movieRuntime = programme.Repertoire.Movie.Runtime;

            var programmeHours = programme.Repertoire.Hours.OrderBy(x => x.Hour).ToArray();
            if (programmeHours.Length == 1)
            {
                return true;
            }

            for (int i = 0; i < programmeHours.Length - 1; i++)
            {
                if (programmeHours[i].Hour.AddMinutes(movieRuntime + 10) > programmeHours[i + 1].Hour)
                {
                    return false;
                }
            }

            return true;
        }

        public DateTime CheckLastRepertoirPlaying(CinemaBl cinema)
        {
            using (var context = _cinemaContext())
            {
                return  AutoMapper.Mapper.Map<Programme, ProgrammeBl>(context.Programme
                    .OrderByDescending(x => x.EndOfPlaying).FirstOrDefault(c => c.Cinema.Id == cinema.Id)).EndOfPlaying;                                                   ;
            }
        }

        public DateTime StartOfPlayingRepertoir(DateTime day)
        {
            DateTime dayOfStartPlayingFilm = DateTime.MinValue;
            do
            {
                try
                {
                    var dayOfWeek = _repertoirService.GetDayNumber(day);

                    dayOfStartPlayingFilm = dayOfWeek < 4 ? day.AddDays((dayOfWeek) * -1) : day.AddDays(dayOfWeek - 1);

                    return dayOfStartPlayingFilm;                 
                }
                catch (Exception)
                {
                    Console.WriteLine("It is not a datetime value. Please enter correct value");
                }

            } while (dayOfStartPlayingFilm.DayOfWeek != DayOfWeek.Friday);
            return dayOfStartPlayingFilm;
        }

        public bool CheckAllEntryHoursIsNotBeforeOpenCinema(ProgrammeBl programme)
        {
            var openingHour = programme.Cinema.OpeningHour;

            foreach (var hour in programme.Repertoire.Hours)
            {
                if (hour.Hour < openingHour)
                {
                    return false;
                }
            }

            return true;
        }


        public ProgrammeBl ReturnProgrammIfExist(int cinemaId,DateTime date, int movieId,int cinemaHallId)
        {
            using (var context = _cinemaContext())
            {
                
                var program =  AutoMapper.Mapper.Map<Programme, ProgrammeBl>(context.Programme
                    .Include(a => a.Cinema)
                    .Include(x => x.Repertoire)
                    .Include(z => z.Repertoire.Hall)
                    .Include(hours => hours.Repertoire.Hours)
                    .Include(b => b.Repertoire.Movie)
                    .Where(x => x.Cinema.Id == cinemaId && DbFunctions.TruncateTime(x.StartOfPlaying) <= DbFunctions.TruncateTime(date)
                                                        && DbFunctions.TruncateTime(x.EndOfPlaying) >= DbFunctions.TruncateTime(date)
                                                        && x.Repertoire.Movie.Id == movieId && x.Repertoire.Hall.Id == cinemaHallId
                                                       
                    ).FirstOrDefault());

                if (program == null)
                {
                    return null;
                }

                return program;
            }
        }
    }
}
