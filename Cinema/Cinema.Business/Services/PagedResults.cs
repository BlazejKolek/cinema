﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.Business.Services
{
   public class PagedResults<T>
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int PagesCount { get; set; }
        public int ResultsCount { get; set; }
        public string NextPageAttributes { get; set; }
        public string PrevPageAttributes { get; set; }

        public IEnumerable<T> Results { get; set; }
    }
}
