﻿using Cinema.Business.Interfaces;
using Cinema.Dal.Data;
using CinemaPlanet.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cinema.Business.Models;

namespace Cinema.Business.Services
{
   public class RepertoirService : IRepertoirService
    {
        private readonly Func<ICinemaContext> _cinemaContext;
        public RepertoirService(Func<ICinemaContext> cinemaContext)
        {
            _cinemaContext = cinemaContext;
        }

        public RepertoireBl CreateNewRepertoir(MovieBl movie,CinemaHallBl hall,List<RepertoirHoursBl> hours)
        {
            using (var context = _cinemaContext())
            {
                int repertoirMaxId;
                try
                {
                   repertoirMaxId = context.Repertoire.Select(x => x.Id).LastOrDefault() ;
                }
                catch (Exception)
                {
                    repertoirMaxId = 0;
                }

                foreach (var hour in hours)
                {
                    hour.RepertoirId = repertoirMaxId + 1;
                }

                var repertoire = new RepertoireBl
                {
                    Id = repertoirMaxId + 1,
                    Movie = movie,
                    Hall = hall,
                    Hours = hours
                };

                return repertoire;
            } 
        }

        public int GetDayNumber(DateTime date)
        {
            switch (date.DayOfWeek)
            {

                case DayOfWeek.Sunday:
                    return 2;
                case DayOfWeek.Monday:
                    return 3;
                case DayOfWeek.Tuesday:
                    return 4;
                case DayOfWeek.Wednesday:
                    return 5;
                case DayOfWeek.Thursday:
                    return 6;
                case DayOfWeek.Friday:
                    return 0;
                case DayOfWeek.Saturday:
                    return 1;
                default:
                    return 99;
            }
        }
    }
}
