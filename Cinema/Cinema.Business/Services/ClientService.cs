﻿using Cinema.Business.Interfaces;
using Cinema.Business.Models;
using Cinema.Dal.Data;
using Cinema.Dal.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cinema.Business.Helpers;
using Cinema.Business.Models.Client;

namespace Cinema.Business.Services
{
    public class ClientService : IClientService
    {
        private readonly Func<ICinemaContext> _cinemaContext;

        public ClientService(Func<ICinemaContext> cinemaContext)
        {
            _cinemaContext = cinemaContext;
        }

        public async Task AddClientAsync(ClientBl client)
        {
            using (var context = _cinemaContext())
            {
                context.Clients.Add(AutoMapper.Mapper.Map<ClientBl, Client>(client));
                await context.SaveChangesAsync();
            }
        }

        public bool CheckDataIsCorrect(ClientBl client)
        {
            if (client == null 
                || string.IsNullOrWhiteSpace(client.Mail)
                || string.IsNullOrWhiteSpace(client.Name)
                || string.IsNullOrWhiteSpace(client.Surname)
                || !EmailValidator.IsEmailValid(client.Mail))
            {
                return false;
            }
            return true;
        }

        public bool IsEmailExist(string email)
        {
            using (var context = _cinemaContext())
            {
                return context.Clients.Any(x => x.Mail.ToLower() == email.ToLower());
            }
        }

        public IEnumerable<ClientBl> GetAllClients()
        {
            using (var context = _cinemaContext())
            {
                return AutoMapper.Mapper.Map<List<Client>, List<ClientBl>>(context.Clients.ToList());
            }
        }

        public ClientDetail AuthorizeClient(ClientAuth client)
        {
            using (var context = _cinemaContext())
            {
                var clientData = context.Clients.FirstOrDefault(x => x.Mail == client.Mail
                                                                 && x.Password.ToUpper() == client.Password.ToUpper());

                if (clientData == null)
                {
                    throw new UnauthorizedAccessException("Invalid mail or password");
                }

                return new ClientDetail
                {
                    Id = clientData.Id,
                    Mail = clientData.Mail,
                    Name = clientData.Name,
                    Surname = clientData.Surname
                };

            }
        }
    }
}
