﻿using Cinema.Dal.Data;
using CinemaPlanet.Business.Interfaces;
using CinemaPlanet.Business.Models;
using CinemaPlanet.Dal.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaPlanet.Business.Services
{
    public class CinemaHallService : ICinemaHallService
    {
        private readonly Func<ICinemaContext> _cinemaContext;

        public CinemaHallService(Func<ICinemaContext> cinemaContext)
        {
            _cinemaContext = cinemaContext;
        }

        public async Task  AddCinemaHallAsync(CinemaHallBl cinemaHall)
        {
            using (var context = _cinemaContext())
            {
                var cinemaHallMappedToDatabase = AutoMapper.Mapper.Map<CinemaHallBl, CinemaHall>(cinemaHall);

                context.Cinema.Attach(cinemaHallMappedToDatabase.Cinema);
                context.Entry(cinemaHallMappedToDatabase.Cinema).State = EntityState.Unchanged;

                context.CinemaHall.Add(cinemaHallMappedToDatabase);
                await context.SaveChangesAsync();
            }
        }

        public bool CheckEntryDataIsCorrect(CinemaHallBl cinemaHall)
        {
            if (cinemaHall == null || cinemaHall.Cinema.Equals(null) 
                      || cinemaHall.Rows.Equals(0) || cinemaHall.Rows > 26 || cinemaHall.Seats.Equals(0))
            {
                return false;
            }

            return true;
        }

        public IEnumerable<CinemaHallBl> GetAllCinemaHallsByCinema(int cinemaId)
        {
            using (var context = _cinemaContext())
            {
                return AutoMapper.Mapper
                    .Map<List<CinemaHall>, List<CinemaHallBl>>
                        (context.CinemaHall.Include(x => x.Cinema)
                        .Where(x => x.Cinema.Id == cinemaId).ToList());
            }
        }

        public CinemaHallBl GetCinemaHallById(int id)
        {
            using (var context = _cinemaContext())
            {
                return AutoMapper.Mapper.Map<CinemaHall, CinemaHallBl>
                (context.CinemaHall.Where(x => x.Id == id)
                    .Include(y => y.Cinema).FirstOrDefault());
            }
        }

        public IEnumerable<CinemaHallBl> GetAllCinemaHalls()
        {
            using (var context = _cinemaContext())
            {
                return AutoMapper.Mapper
                    .Map<List<CinemaHall>, List<CinemaHallBl>>
                    (context.CinemaHall.Include(x => x.Cinema).ToList());
            }
        }
    }
}
