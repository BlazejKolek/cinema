﻿using Cinema.Business.Models;
using Cinema.Business.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.Business.Interfaces
{
   public interface ITicketService
   {
       TicketBl CreateTicket(int cinemaId, string userEmail, int movieId, DateTime date, int cinemaHallId, string row, int seat);
       bool CheckTicketExist(int cinemaId, int movieId, DateTime date, int cinemaHallId, string row,int seat);
       int ReturnTicketPrice(DateTime date);
       bool CheckUserHaveAddSameTicket(List<TicketBl> tickets);
       bool CheckClientHavAddTicketToDifferentProgramms(List<TicketBl> tickets);
       Task<IEnumerable<TicketBl>> GetUserTicketsAsync(int id);
       bool SeatAndRowIsExist(int cinemaHallId, string row, int seat);
       PagedResults<TicketBl> GetTicketPage(int pageSize,int page,int clientId);
   }
}
