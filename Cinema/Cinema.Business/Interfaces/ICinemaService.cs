﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CinemaPlanet.Business.Models;

namespace CinemaPlanet.Business.Interfaces
{
    public interface ICinemaService
    {
        Task AddCinemaAsync(CinemaBl cinema);
        IEnumerable<CinemaBl> GetAllCinemas();
        CinemaBl GetCinemaById(int cinemaId);
        bool CheckDataIsCorrect(CinemaBl cinema);
        //bool IsValidEmail(string emailAddress);
        //bool IsValidTelephoneNumber(string phone);
        DateTime ReturnOpeningHour(CinemaBl cinema);
    }
}