﻿namespace Cinema.Business.Interfaces
{
    public interface IExporterData<T>
    {
        void Serialize(T data, string folderPath);
    }
}
