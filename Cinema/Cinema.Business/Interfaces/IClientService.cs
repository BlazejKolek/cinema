﻿using Cinema.Business.Models;
using Cinema.Business.Models.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.Business.Interfaces
{
   public interface IClientService
   {
       Task AddClientAsync(ClientBl client);
       bool CheckDataIsCorrect(ClientBl client);
       bool IsEmailExist(string email);
       IEnumerable<ClientBl> GetAllClients();
       ClientDetail AuthorizeClient(ClientAuth client);
   }
}
