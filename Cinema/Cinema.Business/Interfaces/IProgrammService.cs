﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cinema.Business.Models;

using CinemaPlanet.Business.Models;

namespace Cinema.Business.Interfaces
{
    public interface IProgrammService
    {
        Task AddProgrammeAsync(ProgrammeBl programme);
        IEnumerable<ProgrammeBl> GetAllProgramme();
        Task<ProgrammeBl> GetProgrammeAsync(int cinema, DateTime date);
        bool CheckEntryDataIsCorrect(ProgrammeBl programme);
        DateTime CheckLastRepertoirPlaying(CinemaBl cinema);
        DateTime StartOfPlayingRepertoir(DateTime date);
        ProgrammeBl ReturnProgrammIfExist(int cinemaId, DateTime date, int movieId, int cinemaHallId);
    }
}