﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cinema.Business.Models;
using CinemaPlanet.Business.Models;

namespace CinemaPlanet.Business.Interfaces
{
    public interface IMovieService
    {
        Task AddMovieAsync(MovieBl movie);
        IEnumerable<MovieBl> GetAllMovies();
        MovieBl ReturnMovieById(int id);
        GenreBl IsGenre(string genre);
        bool CheckEntryDataIsCorrect(MovieBl movie);
    }
}