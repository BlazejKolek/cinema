﻿using CinemaPlanet.Business.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CinemaPlanet.Business.Interfaces
{
    public interface ICinemaHallService
    {
        Task AddCinemaHallAsync(CinemaHallBl cinemaHall);
        IEnumerable<CinemaHallBl> GetAllCinemaHalls();
        CinemaHallBl GetCinemaHallById(int id);
        bool CheckEntryDataIsCorrect(CinemaHallBl cinemaHall);
        IEnumerable<CinemaHallBl> GetAllCinemaHallsByCinema(int cinemaId);
    }
}