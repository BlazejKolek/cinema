﻿using System;
using System.Collections.Generic;

using Cinema.Business.Models;

using CinemaPlanet.Business.Models;

namespace Cinema.Business.Interfaces
{
    public interface IRepertoirService
    {
        RepertoireBl CreateNewRepertoir(MovieBl movie, CinemaHallBl hall,
                                        List<RepertoirHoursBl> hours);
        int GetDayNumber(DateTime date);
    }
}