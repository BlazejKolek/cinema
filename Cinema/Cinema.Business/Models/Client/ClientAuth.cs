﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.Business.Models.Client
{
   public class ClientAuth
    {
        public string Mail { get; set; }
        public string Password { get; set; }
    }
}
