﻿using System;

using Cinema.Business.Models;
using System.Collections.Generic;

namespace CinemaPlanet.Business.Models
{
    public class RepertoireBl
    {
        public int Id { get; set; }
        public MovieBl Movie { get; set; }
        public CinemaHallBl Hall { get; set; }
        public List<RepertoirHoursBl> Hours { get; set; }
    }
}
