﻿using CinemaPlanet.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.Business.Models
{
   public class ProgrammeBl
    {
        public int Id { get; set; }
        public CinemaBl Cinema { get; set; }
        public DateTime StartOfPlaying { get; set; }
        public DateTime EndOfPlaying { get; set; }
        public RepertoireBl Repertoire { get; set; }
    }
}
