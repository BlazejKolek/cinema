﻿using Cinema.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaPlanet.Business.Models
{
    public class CinemaHallBl
    {
        public int Id { get; set; }
        public CinemaBl Cinema { get; set; }
        public int Rows { get; set; }
        public int Seats { get; set; }
    }
}
