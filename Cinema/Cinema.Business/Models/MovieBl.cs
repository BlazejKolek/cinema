﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cinema.Business.Models;

namespace CinemaPlanet.Business.Models
{
    public class MovieBl
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public GenreBl GenreBl { get; set; }
        public int Runtime { get; set; }
        public string ShortDescription { get; set; }
        public string FilmwebPage { get; set; }
    }
}
