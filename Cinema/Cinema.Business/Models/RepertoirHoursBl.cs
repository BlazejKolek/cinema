﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.Business.Models
{
   public class RepertoirHoursBl
    {
        public int Id { get; set; }
        public int RepertoirId { get; set; }
        public DateTime Hour { get; set; }
    }
}
