﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.Business.Models
{
   public class OrderBl
    {
        public int Id { get; set; }
        public List<TicketBl> Tickets { get; set; }
        public double SumOfOrder { get; set; }
    }
}
