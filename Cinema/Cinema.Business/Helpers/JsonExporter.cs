﻿using Cinema.Business.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.Business.Helpers
{
   public class JsonExporter<T> : IExporterData<T>
    {
        public void Serialize(T data, string folderPath)
        {
            var json = JsonConvert.SerializeObject(data);
            Console.WriteLine(json);
            File.WriteAllText(folderPath, json);
        }
    }
}
