﻿using Cinema.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Cinema.Business.Helpers
{
    public class XmlExporter<T> : IExporterData<T>
    {
        public void Serialize(T data, string folderPath)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T),
                                            new XmlRootAttribute());
            StringBuilder stringBuilder = new StringBuilder();
            xmlSerializer.Serialize(new StringWriter(stringBuilder), data);
            File.WriteAllText(folderPath, stringBuilder.ToString());
        }
    }
}
