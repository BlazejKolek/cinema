﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema.Business.Helpers
{
   public static class ValuesComparer
    {
        public static bool CheckListHasSameValues<T>(List<T> values)
        {
            if (values.Count() == 1)
            {
                return false;
            }

            for (int i = 0; i < values.Count - 1; i++)
            {
                for (int j = i + 1; j < values.Count; j++)
                {
                    if (values[i].Equals(values[j]))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
 
}
