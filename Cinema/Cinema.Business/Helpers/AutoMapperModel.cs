﻿using Cinema.Business.Models;
using Cinema.Business.Models.Client;
using Cinema.Dal.Data.Models;
using CinemaPlanet.Business.Models;
using CinemaPlanet.Dal.Data.Models;

namespace CinemaPlanet.Business.Helpers
{
    public static class AutoMapperModel
    {
        public static void CreateMap()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Dal.Data.Models.Cinema, CinemaBl>();
                cfg.CreateMap<CinemaBl, Dal.Data.Models.Cinema>();
                cfg.CreateMap<CinemaHall, CinemaHallBl>();
                cfg.CreateMap<CinemaHallBl, CinemaHall>();
                cfg.CreateMap<Genre, GenreBl>();
                cfg.CreateMap<GenreBl, Genre>();
                cfg.CreateMap<Movie, MovieBl>();
                cfg.CreateMap<MovieBl, Movie>();
                cfg.CreateMap<Repertoire, RepertoireBl>();
                cfg.CreateMap<RepertoireBl, Repertoire>();
                cfg.CreateMap<ProgrammeBl, Programme>();
                cfg.CreateMap<Programme, ProgrammeBl>();
                cfg.CreateMap<RepertoirHoursBl, RepertoirHours>();
                cfg.CreateMap<RepertoirHours, RepertoirHoursBl>();
                cfg.CreateMap<ClientBl, Client>();
                cfg.CreateMap<Client, ClientBl>();
                cfg.CreateMap<TicketBl, Ticket>();
                cfg.CreateMap<Ticket, TicketBl>();
                cfg.CreateMap<OrderBl, Order>();
                cfg.CreateMap<Order, OrderBl>();
                cfg.CreateMap<Client, ClientAuth>();
                cfg.CreateMap<ClientAuth, Client>();
                cfg.CreateMap<Client, ClientDetail>();
                cfg.CreateMap<ClientDetail, Client>();

            });
        }
    }
}
