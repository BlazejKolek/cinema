﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Cinema.Business.Helpers
{
   public static class PhoneValidator
    {
        const string PHONE_NUMBER_REGEX = @"([+]?)([\d]{2}?)([\d]{2})([\d]{7})|([+]?)([\d]{9})|([\d]{9})|([\d]{2})([\d]{7})";

        public static bool IsValidTelephoneNumber(string phone)
        {
            return Regex.IsMatch(phone, PHONE_NUMBER_REGEX);
        }
    }
}
