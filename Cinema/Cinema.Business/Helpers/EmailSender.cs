﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

using Cinema.Business.Interfaces;
using Cinema.Business.Models;
using Cinema.Dal.Data;

namespace Cinema.Business.Helpers
{
   public class EmailSender
   {
       private readonly Func<ICinemaContext> _cinemaContext;


        public EmailSender(Func<ICinemaContext> cinemaContext)
        {
            _cinemaContext = cinemaContext;
        }

       public void SendEmail(OrderBl order)
       {
           using (var context = _cinemaContext())
           {
               var numberOfOrder = context.Orders.Max(x => x.Id);
               string clientEmail = order.Tickets[0].UserEmail;
               var movieId = order.Tickets[0].MovieId;
               var cinemaId = order.Tickets[0].CinemaId;

               var client = context.Clients.FirstOrDefault(x => x.Mail == clientEmail);
               var movie = context.Movie.FirstOrDefault(x =>x.Id == movieId);
               var cinema = context.Cinema.FirstOrDefault(x => x.Id == cinemaId);

                StringBuilder message = new StringBuilder();

                message.Append(@"<html>
                                  <body>
                                  <p><center><b><font size=""12"">Dear " + client.Name + " " + client.Surname + 
                                  @" ,</font></b></center></p>
                                  <p><center><b><font sieze=""10"">Thank you for using the services of our cinema.</font></b></center></p>
                                  <p><b>Your order :</b></p>
                                  <p><b>Cinema</b>  " + String.Join(", ", cinema.Name, cinema.Address) + @"</p>
                                  <p><b>Movie</b> " + movie.Title + @"</p>
                                  <p><b>Date and time of seanse</b> " +
                                  order.Tickets.Select(x => x.TimeOfSeance).FirstOrDefault() + @"</p>");

                foreach (var ticket in order.Tickets)
                {
                    message.AppendFormat($"<p><b>Place {ticket.Row}{ticket.Seat}</b></p>");
                }

                message.Append(@"<p></p>
                                  <p><b>Count of tickets </b>" + order.Tickets.Count + @"</p>
                                  <p><b>Sum of order</b> " + order.SumOfOrder + @"</p>
                                  <p><center><b><font sieze=""10"">Sincerely, <br> Blazej Cinema</br></font></b></center></p>
                                  </body>
                                  </html>
                                  ");

               MailMessage mail = new MailMessage();
               SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

               mail.From = new MailAddress("blazejcinema@gmail.com");
               mail.To.Add(order.Tickets[0].UserEmail);
               mail.Subject = $"Blazej Cinema, Order {numberOfOrder} ";
               mail.Body = message.ToString();
               mail.IsBodyHtml = true;

               SmtpServer.Port = 587;
               SmtpServer.Credentials = new System.Net.NetworkCredential("blazejcinema", "blazejcinema69");
               SmtpServer.EnableSsl = true;

               SmtpServer.Send(mail);
            }

               

        }    
    }
}
