﻿using Cinema.Business.Interfaces;
using Cinema.Business.Services;
using Cinema.Dal.Data;
using CinemaPlanet.Business.Interfaces;
using CinemaPlanet.Business.Services;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaPlanet.Business
{
    public class BusinessModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<ICinemaHallService>().To<CinemaHallService>();
            Kernel.Bind<IMovieService>().To<MovieService>();
            Kernel.Bind<ICinemaService>().To<CinemaService>();
            Kernel.Bind<IRepertoirService>().To<RepertoirService>();
            Kernel.Bind<IProgrammService>().To<ProgrammService>();
            Kernel.Bind<IClientService>().To<ClientService>();
            Kernel.Bind<ITicketService>().To<TicketService>();
            Kernel.Bind<IOrderService>().To<OrderService>();
            
            Kernel.Load(new DalModule());
        }
    }
}
