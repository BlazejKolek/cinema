﻿using CinemaPlanet.Business;
using CinemaPlanet.Business.Interfaces;
using CinemaPlanet.Business.Services;
using CinemaPlanet.Cli.Helpers;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cinema.Cli.Helpers;

namespace CinemaPlanet.Cli
{
    public class CliModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IMenu>().To<Menu>();
            Kernel.Bind<IIoHelper>().To<IoHelper>();
            Kernel.Load(new BusinessModule());
        }
    }
}
