﻿using Cinema.Business.Models;
using CinemaPlanet.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cinema.Cli.Helpers
{
   public class IoHelper : IIoHelper
    {
        public void PrintAllCinemas(IEnumerable<CinemaBl> cinemas)
        {
            foreach (var cinema in cinemas)
            {
                Console.WriteLine($"- Id : {cinema.Id} -  {cinema.Name} in {cinema.Address}");
            }
        }

        public void PrintAllMovies(IEnumerable<MovieBl> movies)
        {
            foreach (var movie in movies)
            {
                Console.WriteLine($"- Id : {movie.Id} - {movie.Title}");
            }
        }

        public void PrintAllHallsFromCinema(IEnumerable<CinemaHallBl> halls)
        {
            foreach (var hall in halls)
            {
                Console.WriteLine($" Cinema hall id : {hall.Id} with " +
                    $"  {hall.Rows} rows and {hall.Seats} seats");
            }
        }

        public void PrintProgrammeByCinemaAndWeek(ProgrammeBl program)
        {
            Console.WriteLine($"Movie {program.Repertoire.Movie.Title} in {program.Cinema.Name} " +
                              $"is a  {program.Repertoire.Movie.GenreBl} " +
                              $"playing from {program.StartOfPlaying} to {program.EndOfPlaying} " +
                              $"in {string.Join(", ",program.Repertoire.Hours.Select(x => x.Hour))} hours " +
                              $"and duration {program.Repertoire.Movie.Runtime} minutes");
        }

        public void PrintGenres()
        {
            string s = string.Join(",", Enum.GetNames(typeof(GenreBl)));

            Console.WriteLine(s);
        }

        public void PrintAllTickets(IEnumerable<TicketBl> tickets)
        {
            foreach (var ticket in tickets)
            {
                Console.WriteLine($"{ticket.Id} buy for {ticket.UserEmail} " +
                                  $"in cinema id in {ticket.CinemaId} play movie id {ticket.MovieId} in cinema hall id {ticket.CinemaHallId} " +
                                  $"- row {ticket.Row} - seat {ticket.Seat} . Time of seanse is {ticket.TimeOfSeance}");
            }
        }
    }
}
