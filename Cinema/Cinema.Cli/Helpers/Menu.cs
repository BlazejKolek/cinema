﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaPlanet.Cli.Helpers
{
    public class Menu : IMenu
    {
        public Dictionary<string, Action> _commands = new Dictionary<string, Action>();

        public void SetCommand(Action command, string commandId)
        {
            if (_commands.ContainsKey(commandId))
            {
                throw new Exception($"Command with id {commandId} is not exist");
            }
            _commands.Add(commandId, command);
        }

        public void Execute(string commandId)
        {
            if (!_commands.ContainsKey(commandId))
            {
                throw new Exception($"Command with id {commandId} is not exist");
            }
            var choosenCommand = _commands[commandId];
            choosenCommand();
        }

        public void PrintAllCommands()
        {
            Console.WriteLine("Available option:");
            foreach (var item in _commands)
            {
                Console.WriteLine($"- {item.Key}");
            }
        }
    }
}
