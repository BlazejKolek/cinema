﻿using System;

namespace CinemaPlanet.Cli.Helpers
{
    public interface IMenu
    {
        void Execute(string commandId);
        void PrintAllCommands();
        void SetCommand(Action command, string commandId);
    }
}