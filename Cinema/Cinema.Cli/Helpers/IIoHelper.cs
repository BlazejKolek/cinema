﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cinema.Business.Models;

using CinemaPlanet.Business.Models;

namespace Cinema.Cli.Helpers
{
    interface IIoHelper
    {
        void PrintAllCinemas(IEnumerable<CinemaBl> cinemas);
        void PrintAllMovies(IEnumerable<MovieBl> movies);
        void PrintAllHallsFromCinema(IEnumerable<CinemaHallBl> halls);
        void PrintProgrammeByCinemaAndWeek(ProgrammeBl programToExport);
        void PrintGenres();
        void PrintAllTickets(IEnumerable<TicketBl> tickets);
    } 
}
