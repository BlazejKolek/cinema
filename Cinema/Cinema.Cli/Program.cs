using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

using Cinema.Business.Helpers;
using Cinema.Business.Interfaces;
using Cinema.Business.Models;
using Cinema.Business.Models.Client;
using Cinema.Business.Services;
using Cinema.Cli.Helpers;
using CinemaPlanet.Business.Helpers;
using CinemaPlanet.Business.Interfaces;
using CinemaPlanet.Business.Models;
using CinemaPlanet.Cli.Helpers;
using Ninject;

namespace CinemaPlanet.Cli
{
   internal class Program
   {
        private readonly IMenu _menu;
        private readonly IMovieService _movieService;
        private readonly ICinemaService _cinemaService;
        private readonly ICinemaHallService _cinemaHallService;
        private readonly IRepertoirService _repertoirService;
        private readonly IProgrammService _programmService;
        private readonly IClientService _clientService;
        private readonly ITicketService _ticketService;
        private readonly IOrderService _orderService;

        private readonly IIoHelper _ioHelper;

        private static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel(new CliModule());
            var run = kernel.Get<Program>();
            run.SetCommandMenu();

            run.ProgramLoop();
        }

        public Program(IMovieService movieService, ICinemaService cinemaService,
                        ICinemaHallService cinemaHallService, IMenu menu, IRepertoirService repertoirService,
                        IProgrammService programmService, IClientService clientService, ITicketService ticketService,
                        IOrderService orderService, IIoHelper ioHelper)
        {
            AutoMapperModel.CreateMap();
            _menu = menu;
            _repertoirService = repertoirService;
            _programmService = programmService;
            _clientService = clientService;
            _ticketService = ticketService;
            _orderService = orderService;
            _ioHelper = ioHelper;
            _movieService = movieService;
            _cinemaService = cinemaService;
            _cinemaHallService = cinemaHallService;
        }

        private void ProgramLoop()
        {
            do
            {
                _menu.PrintAllCommands();
                _menu.Execute(Console.ReadLine());
            }
            while (true);
        }

        private void SetCommandMenu()
        {
            _menu.SetCommand(AddMovie, "Add Movie");
            _menu.SetCommand(AddCinema, "Add cinema");
            _menu.SetCommand(AddCinemaHall, "Add Cinema Hall");
            _menu.SetCommand(AddProgramme, "Add Programme");
            _menu.SetCommand(PrintAllDisplayingProgramm, "Show cinema repertoir");
            _menu.SetCommand(RegisterClient,"Register client");
            _menu.SetCommand(GenerateOrder,"Generate Order");
            _menu.SetCommand(TakeAllTicketForUser,"Take user tickets");
            _menu.SetCommand(Quit, "Quit");
        }

        private void TakeAllTicketForUser()
        {
            try
            {
                var userId = Convert.ToInt32(StringOperation.ReturnTextFormUser("Enter user id"));

                var tickets =  _ticketService.GetUserTicketsAsync(userId).Result;
                if (tickets.Count() == 0)
                {
                    Console.WriteLine($"Not found ticket for user id : {userId}");
                    return;
                }
                _ioHelper.PrintAllTickets(tickets);

            }
            catch (Exception)
            {
                Console.WriteLine("It is not a number");
            }
        }

        private void GenerateOrder()
        {
            Console.WriteLine("Welcome in order generator");

            try
            {
                var userEmail = StringOperation.ReturnTextFormUser("Enter email address");
                if (!_clientService.IsEmailExist(userEmail))
                {
                    Console.WriteLine("Client is not exist. Please register client");
                    return;
                }

                var cinemas = _cinemaService.GetAllCinemas();
                _ioHelper.PrintAllCinemas(cinemas);

                var cinemaId = Convert.ToInt32(StringOperation.ReturnTextFormUser("Enter Cinema Id"));

                var date = Convert.ToDateTime(
                    StringOperation.ReturnTextFormUser(
                        "Enter seanse date. Best format is for example 2011-04-02 12:00"));

                var movies = _movieService.GetAllMovies();
                _ioHelper.PrintAllMovies(movies);

                var movieId = Convert.ToInt32(StringOperation.ReturnTextFormUser("Enter movie Id"));

                var halls = _cinemaHallService.GetAllCinemaHallsByCinema(cinemaId);
                _ioHelper.PrintAllHallsFromCinema(halls);

                var cinemaHallId = Convert.ToInt32(StringOperation.ReturnTextFormUser("Enter cinema hall id"));

                var programmme = _programmService.ReturnProgrammIfExist(cinemaId, date, movieId, cinemaHallId);
                if (programmme == null)
                {
                    Console.WriteLine("There is no possibility to buy tickets when programme doesn't exist.");
                    return;
                }

                var order = new OrderBl();
                var tickets = new List<TicketBl>();

                do
                {

                    char[] aplhabetArray = "ABCDEFGHIJKLMNOPQRSTUVQXYZ".ToCharArray();
                    var availableRows = "";

                    for (int i = 0; i < programmme.Repertoire.Hall.Rows; i++)
                    {
                        availableRows += aplhabetArray[i];
                        availableRows += ",";
                    }

                    availableRows = availableRows.Remove(availableRows.Length - 1);

                    var row = Convert.ToString(StringOperation.ReturnTextFormUser(
                        $"Enter row from existing rows {availableRows}"));

                    var seat = Convert.ToInt32(StringOperation.ReturnTextFormUser(
                        $"Enter seat from existing seats {string.Join(", ", programmme.Repertoire.Hall.Seats)}"));

                    if (!_ticketService.SeatAndRowIsExist(cinemaHallId, row, seat))
                    {
                        Console.WriteLine($"Place for {row} and {seat} isn't exsist.");
                        return;
                    }

                    if (_ticketService.CheckTicketExist(cinemaId,movieId,date, cinemaHallId, row, seat))
                    {
                        Console.WriteLine("Ticket is not available");
                        return;
                    }

                    var ticket = _ticketService.CreateTicket(cinemaId, userEmail, movieId, date, cinemaHallId, row, seat);
                    tickets.Add(ticket);
                }
                while (StringOperation.ReturnTextFormUser("If you want add another ticket enter YES").ToUpper() == "YES");

              var userWanttoAddTheSameTickets =  _ticketService.CheckUserHaveAddSameTicket(tickets);

              var x = ValuesComparer.CheckListHasSameValues(tickets);

              if (userWanttoAddTheSameTickets)
              {
                  Console.WriteLine("User want add the same tickets to order.");
                  return;
              }

               order.Tickets = tickets;
               _orderService.AddOrderAsync(order).Wait();
               Console.WriteLine("Order create. User recive order for you email address.");

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.ToString());
                Console.WriteLine("Wrong input data");
                return;
            }

        }

        private void RegisterClient()
        {
            var email = StringOperation.ReturnTextFormUser("Enter an email");
            if (_clientService.IsEmailExist(email))
            {
                Console.WriteLine("Email exist in database.");
                return;
            }

            var name = StringOperation.ReturnTextFormUser("Enter name");
            var surname = StringOperation.ReturnTextFormUser("Enter surname");

            var client = new ClientBl()
            {
                Name = name,
                Surname = surname,
                Mail = email
            };

            if (!_clientService.CheckDataIsCorrect(client))
            {
                Console.WriteLine("Incorrect data values");
                return;
            }

            _clientService.AddClientAsync(client).Wait();

            Console.WriteLine("Client add to database.");
        }

        private void AddMovie()
        {
            try
            {
                var title = StringOperation.ReturnTextFormUser("Enter movie title");
               
                _ioHelper.PrintGenres();
                var genreToConvert = StringOperation.ReturnTextFormUser("Enter genre");
                var genreReturned = _movieService.IsGenre(genreToConvert);
                GenreBl genre;
                if (genreReturned > 0)
                {
                    genre = genreReturned;
                }
                else
                {
                    Console.WriteLine($"Genre {genreToConvert} is not exist, please enter correct information");
                    return;
                }

                var runtime = Convert.ToInt32(StringOperation.ReturnTextFormUser("Enter runtime in minutes"));
                var shortDesc = StringOperation.ReturnTextFormUser("Enter short description");
                var filmwebAddress = StringOperation.ReturnTextFormUser("Enter filmweb website page");

                var movie = new MovieBl
                {
                    Title = title,
                    GenreBl = genre,
                    Runtime = runtime,
                    ShortDescription = shortDesc,
                    FilmwebPage = filmwebAddress
                };

                if (_movieService.CheckEntryDataIsCorrect(movie))
                {
                    _movieService.AddMovieAsync(movie).Wait();
                }
                else
                {
                    Console.WriteLine("Input data is not correct");
                }
            }
            catch (Exception )
            {
                Console.WriteLine("Input data is not correct");
            }
        }

        private void AddCinema()
        {
            var name = StringOperation.ReturnTextFormUser("Enter cinema name");
            var address = StringOperation.ReturnTextFormUser("Enter city");
            var mail = StringOperation.ReturnTextFormUser("Enter mail");
            var telephone = StringOperation.ReturnTextFormUser("Enter telephone number");

            try
            {

                var openingHour = Convert.ToDateTime(StringOperation.ReturnTextFormUser("Enter opening hour" +
                                                                                        ". Correct value is HH:MM"));
                var cinema = new CinemaBl
                {
                    Name = name,
                    Address = address,
                    Mail = mail,
                    PhoneNumber = telephone,
                    OpeningHour = openingHour
                };

                if (_cinemaService.CheckDataIsCorrect(cinema))
                {
                    _cinemaService.AddCinemaAsync(cinema).Wait();
                }
                else
                {
                    Console.WriteLine("Data entry is incorrect");
                }

            }
            catch (Exception)
            {
                Console.WriteLine("Incorrect value. It's not an hour");
            }
        }

        private void PrintAllDisplayingProgramm()
        {
            var allCinemas = _cinemaService.GetAllCinemas();
            _ioHelper.PrintAllCinemas(allCinemas);

            var getCinemaId = Convert.ToInt32(StringOperation.ReturnTextFormUser("Choose cinema id"));
            var cinema = _cinemaService.GetCinemaById(getCinemaId);
            if (cinema.Id == 0)
            {
                Console.WriteLine($"Cinema with {getCinemaId} id is not exist");
                return;
            }

            try
            {
                var week = Convert.ToDateTime(StringOperation.ReturnTextFormUser("Choose date to " +
                                                                                 "show displaying repertoir"));
                var programToExport = _programmService.GetProgrammeAsync(getCinemaId, week).Result;
                _ioHelper.PrintProgrammeByCinemaAndWeek(programToExport);
            }
            catch (Exception)
            {
                Console.WriteLine("You try enter data which is not a date");
            }
        }

        private void AddCinemaHall()
        {
           var allCinemas =  _cinemaService.GetAllCinemas();
           _ioHelper.PrintAllCinemas(allCinemas);

            try
            {
                var cinemaToGetData = Convert.ToInt32(StringOperation.ReturnTextFormUser("Enter cinema id"));
                var cinema = _cinemaService.GetCinemaById(cinemaToGetData);

                if (cinema == null)
                {
                    Console.WriteLine($"Cinema with id {cinemaToGetData} is not exist");
                    return;
                }

                var rows = Convert.ToInt32(StringOperation.ReturnTextFormUser("Give the number of rows. Max row is 26."));
                var seats = Convert.ToInt32(StringOperation.ReturnTextFormUser("Give the number of sets"));

                var newCinemaHall = new CinemaHallBl { Cinema = cinema, Rows = rows ,Seats = seats};
                if (_cinemaHallService.CheckEntryDataIsCorrect(newCinemaHall))
                {
                     _cinemaHallService.AddCinemaHallAsync(newCinemaHall).Wait();
                }

            }

            catch (Exception)
            {
                Console.WriteLine("Entry data is incorrect");
            }
        }

        private void AddProgramme()
        {
            var allCinemas = _cinemaService.GetAllCinemas();
            _ioHelper.PrintAllCinemas(allCinemas);

            try
            {
                var cinema = _cinemaService.GetCinemaById(
                    Convert.ToInt32(StringOperation.ReturnTextFormUser("Pick cinema id from the list")));

                if (cinema.Id == 0)
                {
                    Console.WriteLine("Cinema is not exist");
                    return;
                }

                DateTime startPlayingRepertoir;
                DateTime endPlayingRepertoir;

                try
                {
                    DateTime lastRepertoirToCinema = _programmService.CheckLastRepertoirPlaying(cinema);
                    startPlayingRepertoir = lastRepertoirToCinema.AddDays(1);
                    endPlayingRepertoir = startPlayingRepertoir.AddDays(6);
                }
                catch (Exception )
                {
                    var day = Convert.ToDateTime(StringOperation
                        .ReturnTextFormUser("Pick day start to play repertoir" +
                                            ".Remember!!! Week start from Friday. " +
                                            "\nAccept date format is MM/DD/YYYY -MM is month, -DD is day and YYYY is Year."));
                    startPlayingRepertoir = _programmService.StartOfPlayingRepertoir(day);
                    endPlayingRepertoir = startPlayingRepertoir.AddDays(6);
                }
                
                var repertoir = AddRepertoirToProgramme(cinema,startPlayingRepertoir,endPlayingRepertoir);

                var programm = new ProgrammeBl
                {
                    Cinema = cinema,
                    StartOfPlaying = startPlayingRepertoir,
                    EndOfPlaying = endPlayingRepertoir,
                    Repertoire = repertoir
                };

                var entryProgrammIsCorrect = _programmService.CheckEntryDataIsCorrect(programm);
                if (entryProgrammIsCorrect)
                {
                     _programmService.AddProgrammeAsync(programm).Wait();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine("Invalid input data");
            }
        }

        private RepertoireBl AddRepertoirToProgramme(CinemaBl cinema,DateTime startPlayingRepertoir,DateTime endPlayingRepertoir)
        {
            var allMovies = _movieService.GetAllMovies();
            _ioHelper.PrintAllMovies(allMovies);

            var movie = _movieService.ReturnMovieById(
                        Convert.ToInt32(StringOperation.ReturnTextFormUser("Pick movie id from the list")));

            var cinemaHall = ReturnCinemaHallToRepertoir(cinema);
            var hours = ReturnHours(cinema , cinemaHall, startPlayingRepertoir, endPlayingRepertoir,movie);

            return _repertoirService.CreateNewRepertoir(movie, cinemaHall, hours);
        }

        private CinemaHallBl ReturnCinemaHallToRepertoir(CinemaBl cinema)
        {
            var hallByCinema = _cinemaHallService.GetAllCinemaHallsByCinema(cinema.Id);
            _ioHelper.PrintAllHallsFromCinema(hallByCinema);

            do
            {
                try
                {
                    var hall = Convert.ToInt32(StringOperation.ReturnTextFormUser("Pick CinemaHall id from the list"));
                    var ciemaHall = _cinemaHallService.GetCinemaHallById(hall);
                    if (ciemaHall == null)
                    {
                        Console.WriteLine($"Pick correct cinema hall. Cinema hall with id {hall} not exist");
                        continue;
                    }
                    return ciemaHall;
                }
                catch (Exception)
                {
                    Console.WriteLine("It is not a number. Pick correct value");
                }
            }
            while (true);
        }

        private List<RepertoirHoursBl> ReturnHours(CinemaBl cinema,CinemaHallBl cinemaHall
                                                    ,DateTime startPlayingRepertoir,DateTime endPlayingRepertoir,MovieBl movie)
        {
            const string hourRegex = @"^(?:0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$";
            var listOfHours = new List<RepertoirHoursBl>();
            
            string endOfAddingHours;
            var openingHour = _cinemaService.ReturnOpeningHour(cinema).TimeOfDay;
            
            do
            {
                var stringAddedHour = StringOperation.ReturnTextFormUser("Enter hour of displaying the movie. " +
                                                                  "Accept format is HH:MM");
                var hour = new RepertoirHoursBl() { Hour = Convert.ToDateTime(stringAddedHour)};
                if (Regex.IsMatch(stringAddedHour, hourRegex))
                {
                    listOfHours.Add(hour);                       
                }
                else
                {
                    Console.WriteLine("It is not an hour");
                }
                endOfAddingHours = StringOperation.ReturnTextFormUser("You wanna add next displaying hour ? Yes or No");
            } while (endOfAddingHours.ToUpper() == "YES");

            return listOfHours;
        }

        private void Quit()
        {
            Console.WriteLine("Good Bye!");
            Thread.Sleep(500);
            Environment.Exit(0);
        }
    }
}
