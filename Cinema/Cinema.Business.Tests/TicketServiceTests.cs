﻿using Cinema.Business.Models;
using Cinema.Business.Services;
using Cinema.Dal.Data;
using Cinema.Dal.Data.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CinemaPlanet.Dal.Data.Models;

namespace Cinema.Business.Tests
{
    [TestFixture]
    public class TicketServiceTests
    {
        [Test]
        public void TicketService_ReturnCorrectPriceForTicket()
        {
            //Arange
            var mondayPrice = 20;
            var saturdayPrice = 25;

            var monday = new DateTime(2019, 05, 06);
            var saturday = new DateTime(2019, 05, 11);

            var ticketService = new TicketService(null, null, null);

            //Act
            var resultForMonday = ticketService.ReturnTicketPrice(monday);
            var resultForSaturday = ticketService.ReturnTicketPrice(saturday);
            //Assert

            Assert.AreEqual(mondayPrice, resultForMonday);
            Assert.AreEqual(saturdayPrice, resultForSaturday);
        }


        [Test]
        public void TicketService_TicketAlreadySold_ReturnFalse()
        {
            //Arrange
            var dbSetMock = new Mock<ICinemaContext>();

            dbSetMock.Setup(x => x.Tickets).Returns(new FakeDbSet<Ticket>
            {
                new Ticket
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2019, 06, 05), CinemaHallId = 1, Row = "C",
                    Seat = 2
                }
            });

            var ticketService = new TicketService(() => dbSetMock.Object, null, null);

            //Act
            var result = ticketService.CheckTicketExist(1, 1, new DateTime(2019, 06, 05), 1, "C", 2);

            //Assert
            Assert.IsTrue(result);

        }

        [Test]
        public void TicketService_UserGaveSameTickets_ReturnFalse()
        {
            //Arange
            var tickets = new List<TicketBl>
            {
                new TicketBl()
                {
                    CinemaHallId = 1, MovieId = 1, Row = "A", Seat = 1, CinemaId = 1,
                    TimeOfSeance = new DateTime(2019, 05, 06), UserEmail = "blazej.kolek@gmail.com"
                },
                new TicketBl()
                {
                    CinemaHallId = 1, MovieId = 1, Row = "A", Seat = 2, CinemaId = 1,
                    TimeOfSeance = new DateTime(2019, 05, 06), UserEmail = "blazej.kolek@gmail.com"
                },
                new TicketBl()
                {
                    CinemaHallId = 1, MovieId = 1, Row = "A", Seat = 1, CinemaId = 1,
                    TimeOfSeance = new DateTime(2019, 05, 06), UserEmail = "blazej.kolek@gmail.com"
                }
            };

            var ticketService = new TicketService(null, null, null);

            //Act

            var result = ticketService.CheckUserHaveAddSameTicket(tickets);
            //Assert

            Assert.IsTrue(result);
        }

        [Test]
        public void TicketService_UserWantToBuyTicketForDifferentUsers_ReturnTrue()
        {
            //Arange
            var tickets = new List<TicketBl>
            {
                new TicketBl()
                {
                    CinemaHallId = 1, MovieId = 1, Row = "A", Seat = 1, CinemaId = 1,
                    TimeOfSeance = new DateTime(2019, 05, 06), UserEmail = "blazej.kolek@gmail.com"
                },
                new TicketBl()
                {
                    CinemaHallId = 1, MovieId = 1, Row = "A", Seat = 2, CinemaId = 1,
                    TimeOfSeance = new DateTime(2019, 05, 06), UserEmail = "blazej.kolek@gmail.com"
                },
            };

            var ticketService = new TicketService(null, null, null);

            //Act

            var result = ticketService.CheckClientsAddedIsTheSame(tickets.Select(x => x.UserEmail).ToList());
            //Assert

            Assert.IsTrue(result);
        }


        [Test]
        public void TIcketService_UserWantToBuyTicketForNotExistingSPlace_ReturnFalse()
        {
            //Arange
            var cinemaHallId = 1;
            string row = "Z";
            int seat = 1;

            var dbSetMock = new Mock<ICinemaContext>();

            dbSetMock.Setup(x => x.CinemaHall).Returns(new FakeDbSet<CinemaHall>
            {
                new CinemaHall()
                {
                    Id = 1,
                    Cinema = null,
                    Rows = 10,
                    Seats = 10
                }
            });

            var ticketService = new TicketService(() => dbSetMock.Object, null, null);

            //Act
            var result = ticketService.SeatAndRowIsExist(cinemaHallId, row, seat);

            //Assert
            Assert.IsFalse(result);
        }
    }
}
