﻿using Cinema.Business.Models;
using Cinema.Business.Services;
using CinemaPlanet.Business.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CinemaPlanet.Business.Interfaces;
using CinemaPlanet.Business.Services;

using Moq;

namespace Cinema.Business.Tests
{ 
    [TestFixture]
    public class ProgrammeServiceTests
    {
        public ProgrammeBl PrgrammeToTestWithWrongOpeningHour()
        {
            DateTime today = DateTime.Today;
            RepertoirHoursBl hour = new RepertoirHoursBl()
                {Hour = new DateTime(today.Year, today.Month, today.Day, 10, 00, 00)};
            var programme = new ProgrammeBl()
            {
                Id = 1,
                Cinema = new CinemaBl() { Id = 1, Address = "Gdansk", Mail = "planet@gmail.com", Name = "CinemaPlanet", PhoneNumber = "503111222", OpeningHour = Convert.ToDateTime("11:00") },
                StartOfPlaying = Convert.ToDateTime("06/06/2019"),
                EndOfPlaying = Convert.ToDateTime("12/06/2019"),

                Repertoire = new RepertoireBl()
                {
                    Movie = new MovieBl() { Id = 1 },
                    Hall = new CinemaHallBl { Id = 1 },
                    Hours = new List<RepertoirHoursBl>() {hour}
                }
            };
            return programme;
        }

        public ProgrammeBl CorrectProgrammeToTest()
        {
            DateTime today = DateTime.Today;
            RepertoirHoursBl hour = new RepertoirHoursBl()
                { Hour = new DateTime(today.Year, today.Month, today.Day, 10, 00, 00) };

            var programme = new ProgrammeBl()
            {
                Id = 1,
                Cinema = new CinemaBl() { Id = 1, Address = "Gdansk", Mail = "planet@gmail.com", Name = "CinemaPlanet", PhoneNumber = "503111222", OpeningHour = Convert.ToDateTime("06:00") },
                StartOfPlaying = Convert.ToDateTime("06/06/2019"),
                EndOfPlaying = Convert.ToDateTime("12/06/2019"),

                Repertoire = new RepertoireBl()
                {
                    Movie = new MovieBl() { Id = 1 },
                    Hall = new CinemaHallBl { Id = 1 },
                    Hours = new List<RepertoirHoursBl>() { hour }
                }
            };
            return programme;
        }

        public ProgrammeBl ProgrammWithOverlappingHours()
        {
            DateTime today = DateTime.Today;
            RepertoirHoursBl firsAddedHour = new RepertoirHoursBl()
                { Hour = new DateTime(today.Year, today.Month, today.Day, 10, 00, 00) };

            RepertoirHoursBl secondAddedHour = new RepertoirHoursBl()
                { Hour = new DateTime(today.Year, today.Month, today.Day, 11, 00, 00) };

            var programme = new ProgrammeBl()
            {
                Id = 1,
                Cinema = new CinemaBl() { Id = 1, Address = "Gdansk", Mail = "planet@gmail.com", Name = "CinemaPlanet", PhoneNumber = "503111222", OpeningHour = Convert.ToDateTime("06:00") },
                StartOfPlaying = Convert.ToDateTime("06/06/2019"),
                EndOfPlaying = Convert.ToDateTime("12/06/2019"),

                Repertoire = new RepertoireBl()
                {
                    Movie = new MovieBl() { Id = 1,Runtime = 120},
                    Hall = new CinemaHallBl { Id = 1 },
                    Hours = new List<RepertoirHoursBl>() { firsAddedHour,secondAddedHour}
                }
            };
            return programme;
        }

        [Test]
        public void CheckInputHourIsBeforeOpen_ValidInput_InvalidOutput()
        {
            //Arange
            var programmeService = new ProgrammService(null, null, null);
            var programme = PrgrammeToTestWithWrongOpeningHour();

            //Act
            var result = programmeService.CheckAllEntryHoursIsNotBeforeOpenCinema(programme);

            //Assert

            Assert.IsFalse(result);
        }

        [Test]
        public void CheckValidation_ValidInput_ValidOutput()
        {
            //Arange

            var cinema = new CinemaBl
            {
                Name = "Multikino",
                Address = "Gdynia",
                Mail = "multi@gmail.com",
                PhoneNumber = "502099099",
                OpeningHour = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 10, 00, 00)
            };

            var hour = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 10, 00, 00);
            var cinemaService = new Mock<ICinemaService>();
            var programmeService = new ProgrammService(null, null, cinemaService.Object);
            var programme = CorrectProgrammeToTest();


            //Act
            var result = programmeService.CheckEntryDataIsCorrect(programme);

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void CheckValidationWhenHoursIsOverlappingReturnsFalse()
        {
            //Arange

            var cinema = new CinemaBl();

            var hour = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 10, 00, 00);
            var cinemaService = new Mock<ICinemaService>();
            cinemaService.Setup(x => x.ReturnOpeningHour(cinema)).Returns(hour);
            var programmeService = new ProgrammService(null, null, cinemaService.Object);
            var programme = ProgrammWithOverlappingHours();


            //Act
            var result = programmeService.CheckAllHoursGivenIsOverlapping(programme);

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void CheckWhenEntryDataIsNull()
        {
            //Arange
            var programmeService = new ProgrammService(null, null, null);
            var programme = new ProgrammeBl();
           
            //Act
            var result = programmeService.CheckEntryDataIsCorrect(null);

            //Assert
            Assert.IsFalse(result);
        }
    }
}
