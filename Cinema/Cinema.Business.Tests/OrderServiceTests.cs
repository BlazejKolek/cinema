﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cinema.Business.Models;
using Cinema.Business.Services;
using Cinema.Dal.Data;
using Cinema.Dal.Data.Models;
using Moq;
using NUnit.Framework;

namespace Cinema.Business.Tests
{
    [TestFixture]
   public class OrderServiceTests
    {
        [Test]
        public void OrderService_ClientDontHaveAvailablePromotionToCurrentOrder_ReturnUnchangedObjects()
        {
            //Arrange
            var dbSetMock = new Mock<ICinemaContext>();

            dbSetMock.Setup(x => x.Tickets).Returns(new FakeDbSet<Ticket>
            {
                new Ticket
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2019, 06, 05), CinemaHallId = 1, Row = "C",
                    Seat = 2,SaleDate = DateTime.Today,Price = 20,UserEmail = "blazej.kolek@gmail.com"
                },
                new Ticket
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2019, 06, 05), CinemaHallId = 1, Row = "D",
                    Seat = 4,SaleDate = DateTime.Today,Price = 20,UserEmail = "blazej.kolek@gmail.com"
                }             
            });


            var tickets = new List<TicketBl>()
            {
                new TicketBl
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2019, 06, 05), CinemaHallId = 1, Row = "G",
                    Seat = 2, SaleDate = DateTime.Today, Price = 24, UserEmail = "blazej.kolek@gmail.com"
                },
                new TicketBl
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2019, 06, 05), CinemaHallId = 1, Row = "G",
                    Seat = 5, SaleDate = DateTime.Today, Price = 24, UserEmail = "blazej.kolek@gmail.com"
                }
            };



            var orderService = new OrderService(() => dbSetMock.Object,null);

            //Act
            var result = orderService.ChangePriceWhenPromotionIsAvailabel(tickets);

            //Assert

            for (int i = 0; i < result.Count; i++)
            {
                Assert.AreEqual(result[i].Price,tickets[i].Price);
            }
        }


        [Test]
        public void OrderService_ClienttHaveAvailablePromotionToCurrentOrder_10_TicketFree_ReturnUnchangedObjectsWithFreeTicket()
        {
            //Arrange
            var dbSetMock = new Mock<ICinemaContext>();

            dbSetMock.Setup(x => x.Tickets).Returns(new FakeDbSet<Ticket>
            {
                new Ticket
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2018, 06, 05), CinemaHallId = 1, Row = "C",
                    Seat = 2,SaleDate = DateTime.Today,Price = 20,UserEmail = "blazej.kolek@gmail.com"
                },
                new Ticket
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2018, 06, 05), CinemaHallId = 1, Row = "D",
                    Seat = 4,SaleDate = DateTime.Today,Price = 20,UserEmail = "blazej.kolek@gmail.com"
                },
                new Ticket
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2018, 06, 05), CinemaHallId = 1, Row = "G",
                    Seat = 5, SaleDate = DateTime.Today, Price = 24, UserEmail = "blazej.kolek@gmail.com"
                },
                new Ticket
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2018, 06, 05), CinemaHallId = 1, Row = "H",
                    Seat = 5, SaleDate = DateTime.Today, Price = 24, UserEmail = "blazej.kolek@gmail.com"
                },
                new Ticket
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2018, 06, 05), CinemaHallId = 1, Row = "I",
                    Seat = 5, SaleDate = DateTime.Today, Price = 24, UserEmail = "blazej.kolek@gmail.com"
                },
                new Ticket
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2018, 06, 05), CinemaHallId = 1, Row = "J",
                    Seat = 5, SaleDate = DateTime.Today, Price = 24, UserEmail = "blazej.kolek@gmail.com"
                },
                new Ticket
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2018, 06, 05), CinemaHallId = 1, Row = "A",
                    Seat = 5, SaleDate = DateTime.Today, Price = 24, UserEmail = "blazej.kolek@gmail.com"
                },
                new Ticket
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2018, 06, 05), CinemaHallId = 1, Row = "B",
                    Seat = 5, SaleDate = DateTime.Today, Price = 24, UserEmail = "blazej.kolek@gmail.com"
                },
                new Ticket
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2018, 06, 05), CinemaHallId = 1, Row = "A",
                    Seat = 8, SaleDate = DateTime.Today, Price = 24, UserEmail = "blazej.kolek@gmail.com"
                }
            });


            var tickets = new List<TicketBl>()
            {
                new TicketBl
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2019, 06, 05), CinemaHallId = 1, Row = "G",
                    Seat = 2, SaleDate = DateTime.Today, Price = 24, UserEmail = "blazej.kolek@gmail.com"
                }             
            };

            var freeTicketPrice = 0;

            var orderService = new OrderService(() => dbSetMock.Object,null);

            //Act
            var result = orderService.ChangePriceWhenPromotionIsAvailabel(tickets);

            //Assert

            for (int i = 0; i < result.Count; i++)
            {
                Assert.AreEqual(result[i].Price, freeTicketPrice);
            }
        }


        [Test]
        public void OrderService_ClientHaveAvailablePromotionTo_MoreThanFiveTIcketSaleInCurrentMonth_ReturnUnchangedObjects()
        {
            //Arrange
            var dbSetMock = new Mock<ICinemaContext>();

            dbSetMock.Setup(x => x.Tickets).Returns(new FakeDbSet<Ticket>
            {
                new Ticket
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2019, 06, 05), CinemaHallId = 1, Row = "C",
                    Seat = 2,SaleDate = DateTime.Today,Price = 20,UserEmail = "blazej.kolek@gmail.com"
                },
                new Ticket
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2019, 06, 05), CinemaHallId = 1, Row = "D",
                    Seat = 4,SaleDate = DateTime.Today,Price = 20,UserEmail = "blazej.kolek@gmail.com"
                },
                new Ticket
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2019, 06, 05), CinemaHallId = 1, Row = "G",
                    Seat = 5, SaleDate = DateTime.Today, Price = 24, UserEmail = "blazej.kolek@gmail.com"
                },
                new Ticket
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2019, 06, 05), CinemaHallId = 1, Row = "H",
                    Seat = 5, SaleDate = DateTime.Today, Price = 24, UserEmail = "blazej.kolek@gmail.com"
                },
                new Ticket
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2019, 06, 05), CinemaHallId = 1, Row = "I",
                    Seat = 5, SaleDate = DateTime.Today, Price = 24, UserEmail = "blazej.kolek@gmail.com"
                }
            });


            var tickets = new List<TicketBl>()
            {
                new TicketBl
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2019, 06, 05), CinemaHallId = 1, Row = "G",
                    Seat = 2, SaleDate = DateTime.Today, Price = 20, UserEmail = "blazej.kolek@gmail.com"
                },
                new TicketBl
                {
                    CinemaId = 1, MovieId = 1, TimeOfSeance = new DateTime(2019, 06, 05), CinemaHallId = 1, Row = "G",
                    Seat = 1, SaleDate = DateTime.Today, Price = 20, UserEmail = "blazej.kolek@gmail.com"
                }
            };

            var discountPrice = tickets.FirstOrDefault().Price * 0.8 ;

            var orderService = new OrderService(() => dbSetMock.Object,null);

            //Act
            var result = orderService.ChangePriceWhenPromotionIsAvailabel(tickets);

            //Assert

            for (int i = 0; i < result.Count; i++)
            {
                Assert.AreEqual(result[i].Price, discountPrice);
            }
        }

    }
}
